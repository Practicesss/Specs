#
# Be sure to run `pod lib lint PracticesssLoginModule.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    # 私有库名称
    s.name = 'PracticesssLoginModule'
    
    # 版本号
    s.version = '0.0.1'
    
    # 简介
    s.summary = '测试用CocoaPod组件化开发组件'
    
    # 介绍，不得比简介短，否则可能会无法提交
    s.description = <<-DESC
    测试用CocoaPod组件化开发组件
    DESC
    
    # GitLab主页
    s.homepage = 'https://gitlab.com/prewindemon/PracticesssLoginModule'
    s.license = { :type => 'MIT', :file => 'LICENSE' }
    s.author = { 'xxxxxx' => 'xxxxx@xxx.com' }
    
    
    # GitLab源
    s.source = { :git => 'https://gitlab.com/prewindemon/PracticesssLoginModule.git', :tag => s.version }
    
    # 最低支持版本
    s.ios.deployment_target = '8.0'
    
    # Pod文件
    s.source_files = 'PracticesssLoginModule/Classes/**/*'
    
    # 头文件
    # s.public_header_files = 'PracticesssLoginModule/Classes/Header/**/*.h'
    
    # Pch文件默认内容
    #s.prefix_header_contents = <<-EOS
    #   #import "PracticesssLoginModule.h"
    #EOS
    
    # 静态库依赖
    # s.ios.vendored_libraries = 'PracticesssLoginModule/Library/*.a'
    
    # 依赖Lib
    # s.libraries = 'z','xml2'
end
